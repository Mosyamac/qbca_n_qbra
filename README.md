**Query-Based Classification and Regression algoritms**

This repository contains R source code for Query-Based Classification and Regression algoritms introduced in Ph.D. thesis ["Interval Pattern Structures Randomized Algorithms for Classification and Regression Tasks in Credit Risk Management"](https://www.hse.ru/sci/diss/220944851) by Alexey Masyutin.

Those are two interpretable Data Mining algorithms that produce prediction for test objects on individual basis, avoiding construction of one-fits-all model. 
The algorithms can be applied to credit scoring and loss given default problems.

1. Subdirectory "data" contains demo datasets to execute a simple how-to snippet of code.
2. "launcher_QBCA.R" is an example for classification algorithm.
3. "launcher_QBRA.R" is an example for regression algorithm.

Mind that code requires pROC and Hmisc packages installed on your machine (they are installed if needed in launchers).

---

If you have any questions you can drop a letter to **alexey.masyutin@gmail.com**